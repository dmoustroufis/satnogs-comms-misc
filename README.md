# Misc

## A100-AT86-LVDS-breakout

Breakout for connecting Arty-7 FPGA devboard with ATREB215-xpro Extension Board

## at86-LVDS-test

Artix-7 FPGA project in vivado for testing AT96RF215 I/Q

## AGC Development Board

AGC Development Board is based on VGA ADL5330 and detector AD8318.
Requires [lsf-kicad-lib](https://gitlab.com/librespacefoundation/lsf-kicad-lib) to be installed.

## RF Mixer with integrated LO

RF mixer development board with integrated LO. The board is based on RFFC2071.
Requires [lsf-kicad-lib](https://gitlab.com/librespacefoundation/lsf-kicad-lib) to be installed.

## LNA for UHF
LNA for UHF, based on TQP3M9036 and (optionally) on BPF Taoglas DBP.433.T.A.30.
Also the board contains ESD protection (optionally), Littelfuse PGB1010603MR.
Requires [lsf-kicad-lib](https://gitlab.com/librespacefoundation/lsf-kicad-lib) to be installed.

## RF Probe
RF Probe, it is used to calibrate and debug RFFE of SatNOGS COMMS Board.
Instead to get input/output in AT86RF215 it gets input/output in SA/SG.
Requires [lsf-kicad-lib](https://gitlab.com/librespacefoundation/lsf-kicad-lib) to be installed.

## License
Licensed under the [CERN OHLv1.2](LICENSE)
[![Libre Space Foundation](https://img.shields.io/badge/%C2%A9%202014--2020-Libre%20Space%20Foundation-6672D8.svg)](https://librespacefoundation.org/)
