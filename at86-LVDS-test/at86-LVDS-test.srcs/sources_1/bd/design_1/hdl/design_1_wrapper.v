//Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
//Date        : Wed Mar 11 19:32:12 2020
//Host        : drid-t420s running 64-bit Ubuntu 18.04.4 LTS
//Command     : generate_target design_1_wrapper.bd
//Design      : design_1_wrapper
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module design_1_wrapper
   (RX09_N,
    RX09_P,
    RX24_N,
    RX24_P,
    RXCLK_N,
    RXCLK_P,
    TXCLK_N,
    TXCLK_P,
    TXD_N,
    TXD_P,
    reset,
    sys_clock);
  input [0:0]RX09_N;
  input [0:0]RX09_P;
  input [0:0]RX24_N;
  input [0:0]RX24_P;
  input RXCLK_N;
  input RXCLK_P;
  output [0:0]TXCLK_N;
  output [0:0]TXCLK_P;
  output [0:0]TXD_N;
  output [0:0]TXD_P;
  input reset;
  input sys_clock;

  wire [0:0]RX09_N;
  wire [0:0]RX09_P;
  wire [0:0]RX24_N;
  wire [0:0]RX24_P;
  wire RXCLK_N;
  wire RXCLK_P;
  wire [0:0]TXCLK_N;
  wire [0:0]TXCLK_P;
  wire [0:0]TXD_N;
  wire [0:0]TXD_P;
  wire reset;
  wire sys_clock;

  design_1 design_1_i
       (.RX09_N(RX09_N),
        .RX09_P(RX09_P),
        .RX24_N(RX24_N),
        .RX24_P(RX24_P),
        .RXCLK_N(RXCLK_N),
        .RXCLK_P(RXCLK_P),
        .TXCLK_N(TXCLK_N),
        .TXCLK_P(TXCLK_P),
        .TXD_N(TXD_N),
        .TXD_P(TXD_P),
        .reset(reset),
        .sys_clock(sys_clock));
endmodule
